const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const path = require('path');

const posts = require('./routes/post');

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.all('*', function(req, res, next) {
  res.set('Access-Control-Allow-Origin', '*');
  res.set('Access-Control-Allow-Credentials', true);
  res.set('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
  res.set('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Authorization');
  if ('OPTIONS' == req.method) return res.sendStatus(200);
  next();
});



// db config
const db = require('./config/key');

// connect to mongodb
mongoose
  .connect(db.mongoURI, {useNewUrlParser : true, useUnifiedTopology: true })
  .then(() => console.log('mongoDB connected'))
  .catch(err => console.log(err));


// Use Routes
app.use("/uploads", express.static(path.join(__dirname, 'uploads')));
app.use('/routes/post', posts);
app.use('/', express.static(path.join(__dirname, '')));


const port = process.env.PORT || 5000;

app.listen(port, () => console.log('server is runing'));
