const express = require('express');
const router = express.Router();
const multer = require('multer');
const fs = require('fs');
const Post = require('../models/post');


const fileFilter = (req, file, cb) => {
    if (file.mimetype == 'image/jpeg' || file.mimetype == 'image/png' || file.mimetype == 'text/csv') {
        cb(null, true);
    } else {
        cb(null, false);
    }
}

var storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null,'uploads');
  },
  filename: function(req, file, cb) {
    cb(null, file.originalname);
  }
});
const upload = multer({
  storage: storage,
  fileFilter: fileFilter
})


//get all post
router.get('/getall', (req, res) => { 
  const post = Post.find()
  .select('userId title body type tag userName path temp description')
  .then(post =>{
    res.json({post});

  })
  .catch(err => console.log(err,'not found'));
  });


// Post api for image upload
router.post('/imageUpload', upload.single('file'), (req, res, cb) => {
  req.body.path = req.file.destination + req.file.originalname;
  req.body.temp = req.file.originalname;
   Post.create(req.body).then(result => {
       res.status(200).json(result)
   }).catch(err => {
       res.status(404).json(err.message);
   });
});

// Post api for Video upload
router.post('/videoUpload', upload.single('file'), (req, res, cb) => {
    req.body.path = req.file.destination + req.file.originalname;
    req.body.temp = req.file.originalname;
     Post.create(req.body).then(result => {
         res.status(200).json(result)
     }).catch(err => {
         res.status(404).json(err.message);
     });
});

// get particular image by id
router.get('/images/:id', (req, res) => {
    const post = Post.findOne()
    .select('userId title body type tag userName path temp description')
    .then(post =>{
      res.json({post});
  
    })
    .catch(err => console.log(err,'not found'));
  
});

// get particular Video by id
router.get('/video/:id', (req, res) => {
    const post = Post.find()
  .select('userId title body type tag userName path temp description')
  .then(post =>{
    res.json({post});

  })
  .catch(err => console.log(err,'not found'));
    
  });


// Post Api for project upload
router.post('/csvUpload', (req, res,cb) => {
    req.body.path = req.file.destination + req.file.originalname;
    req.body.temp = req.file.originalname;
     Post.create(req.body).then(result => {
         res.status(200).json(result)
     }).catch(err => {
         res.status(404).json(err.message);
     });
});

// get particular project by id
router.get('/csv/:id', (req, res) => {
    const post = Post.find()
    .select('userId title body type tag userName path temp description')
    .then(post =>{
      res.json({post});
  
    })
    .catch(err => console.log(err,'not found'));
    
  });


// Delete api for Images
router.delete('/images/:id', (req, res) => {
  
  Post.findByIdAndRemove(req.params.id)
    .then(post => {
      if (!post) {
        return res.status(404).send({
          message: 'Post not found with id ' + req.params.imageId
        });
      }
      res.send({ message: 'Post deleted successfully!' });
    })
    .catch(err => {
      if (err.kind === 'ObjectId' || err.name === 'NotFound') {
        return res.status(404).send({
          message: 'Post not found with id ' + req.params.imageId
        });
      }
      return res.status(500).send({
        message: 'Could not delete image with id ' + req.params.imageId
      });
    });
});

// Delete api for Video
router.delete('/video/:id', (req, res) => {
  
    Post.findByIdAndRemove(req.params.id)
      .then(post => {
        if (!post) {
          return res.status(404).send({
            message: 'Post not found with id ' + req.params.videoId
          });
        }
        res.send({ message: 'Post deleted successfully!' });
      })
      .catch(err => {
        if (err.kind === 'ObjectId' || err.name === 'NotFound') {
          return res.status(404).send({
            message: 'Post not found with id ' + req.params.videoId
          });
        }
        return res.status(500).send({
          message: 'Could not delete video with id ' + req.params.videoId
        });
      });
  });

  // Delete api for project
router.delete('/csv/:id', (req, res) => {
  
    Post.findByIdAndRemove(req.params.id)
      .then(post => {
        if (!post) {
          return res.status(404).send({
            message: 'Post not found with id ' + req.params.projectId
          });
        }
        res.send({ message: 'Post deleted successfully!' });
      })
      .catch(err => {
        if (err.kind === 'ObjectId' || err.name === 'NotFound') {
          return res.status(404).send({
            message: 'Post not found with id ' + req.params.projectId
          });
        }
        return res.status(500).send({
          message: 'Could not delete project with id ' + req.params.projectId
        });
      });
  });

//Update Video

router.put('/video/:id', (req, res) => {
  console.log(req.body)
    Post.findByIdAndUpdate({_id:req.params.id},{$set: req.body})
      .then(post => {
        if (!post) {
          return res.status(404).send({
            message: 'Post not found with id ' + req.params.videoId
          });
        }
        res.send({ message: 'Post updated successfully!' });
      })
      .catch(err => {
        if (err.kind === 'ObjectId' || err.name === 'NotFound') {
          return res.status(404).send({
            message: 'Post not found with id ' + req.params.videoId
          });
        }
        return res.status(500).send({
          message: 'Could not update images field with id ' + req.params.videoId
        });
      });
  });

//update image



router.put('/images/:id', (req, res) => {
    console.log(req.body)
      Post.findByIdAndUpdate({_id:req.params.id},{$set: req.body})
        .then(post => {
          if (!post) {
            return res.status(404).send({
              message: 'Post not found with id ' + req.params.videoId
            });
          }
          res.send({ message: 'Post updated successfully!' });
        })
        .catch(err => {
          if (err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
              message: 'Post not found with id ' + req.params.videoId
            });
          }
          return res.status(500).send({
            message: 'Could not update images field with id ' + req.params.videoId
          });
        });
    });


    //Update CSV File

router.put('/csv/:id', (req, res) => {
    console.log(req.body)
      Post.findByIdAndUpdate({_id:req.params.id},{$set: req.body})
        .then(post => {
          if (!post) {
            return res.status(404).send({
              message: 'Post not found with id ' + req.params.videoId
            });
          }
          res.send({ message: 'Post updated successfully!' });
        })
        .catch(err => {
          if (err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
              message: 'Post not found with id ' + req.params.videoId
            });
          }
          return res.status(500).send({
            message: 'Could not update CSV fiel with id ' + req.params.videoId
          });
        });
    });


  // Post api for multi Image upload
router.post('/multiUpload', upload.array("file", 4),  (req, res, cb) => {
    
    req.files.map(result => {
        console.log(result , '+++++')
    })
    req.body.path = '/uploads/' + req.file.originalname;
    req.body.temp = req.file.originalname;
     Post.create(req.body).then(result => {
         res.status(200).json(result)
     }).catch(err => {
         res.status(404).json(err.message);
     });
  });

module.exports = router;
