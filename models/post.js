const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// create UserSchema
const PostSchema = new Schema({
  title: {
    type: String,
    
  },
  userName:{
    type:String
  },
  location: {
    type: String,
    
  },
  name: {
    type: String,
    require: true
  },
  phase: {
    type: String,
    
  },
  description: {
    type: String
  },
  tag: {
    type: String
  },
  type: {
    type: String
  },
  userId: {
    type: String,
  },
  evnetdate: {
    type: Date,
    default: Date.now
  },
  path: {
    type: String
  },
  temp: {
    type: String
  },
  date: {
    type: Date,
    default: Date.now
  },
  
  
 
});

module.exports = User = mongoose.model('post', PostSchema);
